import React from 'react'
import {send } from 'emailjs-com';
import './App.css'
import contactezNous from "./Image/contactezNous.jpg";
import iconeAppel from "./Image/iconeAppel.png";
import iconeLocation from "./Image/iconeLocation.jpg";
import iconeMail from "./Image/iconeMail.png"



const Contact = () => {


    const [nomEntree, setNomEntree] = React.useState('');
    const [prenomEntree, setPrenomEntree] = React.useState('');
    
    const [emailEntree, setEmailEntree] = React.useState('');
    const [texteEntree, setTexteEntree] = React.useState('');
    
    
    const handleNom = event => {
        setNomEntree(event.target.value);      
    };

    const handlePrenom = event => {
        setPrenomEntree(event.target.value);      
    };

    const handleEmail = event => {
        setEmailEntree(event.target.value);      
    };

    const handleTexte = event => {
        setTexteEntree(event.target.value);      
    };

    const onSubmit = async () => {
        try {
          const templateParams = {
            nomEntree,
            prenomEntree,
            emailEntree,
            texteEntree
          };    
          await send(
            "Portfolio",
           "template_y9tqsba",
            templateParams,
            "user_T9mEjcgxHjWKcqphdOOzz"
          );
          alert("Mail Envoyé");
        } catch (e) {
          console.log(e);
        }
      };

    return (
        <aside className="contacter">
            <div className="contact-top">
                <div className="contact-top-bottom">
                    <div className="contact-top-bottom-titre">
                        <p style={{fontSize:"11vh"}}><span style={{fontWeight:"bold", color:"#F08080", textAlign:"center"}}>Contact</span></p>
                            <ul className="contact-top-bottom-option">
                                <div className="contact-top-bottom-acceuil">
                                    <li>Acceuil</li>
                                </div>
                                <div className="contact-top-bottom-contact">
                                    <li>Contact</li>
                                </div>
                            </ul>
                    </div>
                </div>
            </div>
            <div className="contact-middle">
                <div className="contact-middle-Texte">
                    <div className ="contact-middle-Texte1">
                        <p className="text_contacter_moi_hoje">Contacter moi aujourd'hui</p>
                    </div>
                    <div className ="contact-middle-Texte2">
                        <p><span className="texte_construisez_hoje">Construisez de beaux sites web Moderne et adaptatif</span></p>     
                    </div>
                    <div className ="contact-middle-Texte3">
                        <p className="text_contact">Plusieurs varrations de ce site sont disponible afin d'en montrer son evolution</p>
                    </div>
                </div>
                <div className = "container">
                        <div className="contact-middle-carte">
                            <div className="contact-middle-left-icone">
                                <img className="contact-middle-images" src={iconeAppel} style={{width:"6vh",margin:"1vh", marginTop:"2.5vh"}} alt="iconeAppel"/>
                            </div>
                            <div className="contact-middle-left-infoAppel">
                                <div className="contact-middle-left-texte">
                                    <p><span className="contact-middle-left-texte">M'appeler</span></p>
                                </div>
                                <div className ="contact-numero">
                                    <p>Mobile: +33 6 51 56 93 46</p>                           
                                </div>
                            </div>
                        </div>
                        <div className="contact-middle-carte">
                                <div className="contact-middle-middle-icone">
                                    <img className="contact-middle-images" src={iconeMail} style={{width:"7vh",margin:"1vh", marginTop:"2vh"}} alt="iconeMail"></img>
                                </div>
                                <div className="contact-middle-middle-textail">
                                    <div className="contact-middle-middle-texte">
                                        <p ><span className="contact-middle-middle-texte">M'envoyer un Email</span></p>
                                    </div>
                                    <div className="contact-middle-middle-emailpro">                                
                                        <p>vbakayoko0@gmail.com</p>
                                        <p>vafing084@gmail.com</p>
                                    </div>
                                </div>

                        </div>
                        <div className="contact-middle-carte">
                            <div className="contact-middle-right-icone">
                                <img className="contact-middle-images" src={iconeLocation} style={{width:"6vh",margin:"1vh", marginTop:"2.4vh"}} alt="iconeLocation" ></img>
                            </div>
                            <div className="contact-middle-right-infolocation">
                                <div className="contact-middle-infollocation">
                                    <p><span className="contact-middle-infollocation">Location</span></p>
                                </div>
                                <div className="contact-middle-right-codepostal">
                                    <p>38100 Grenoble</p>
                                </div>
                                <div className="contact-middle-right-addresse">
                                    <p>1 Avenue Paul Cocat</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div className="contact-bottom">
                <div className="contact-bottom-left">
                    <p><span className="texte_construisez_hoje">Contactez Moi</span></p>
                    <div>
                        <p className="text_contact">Je suis disponible pour du freelance. Vous pouvez me joindre sur le<span style={{fontWeight: "bold"}}> +33 06 51 56 93 46</span> ou par mail via <span style={{fontWeight: "bold"}}>vbakayoko0@gmail.com</span>.</p>
                    </div>
                    <div className="contact">
                    <form>
                        <div className="row">
                            <div className="col">
                            <Nom onNom={handleNom} nom={nomEntree}/>
                            </div>

                            <div className="col">
                            <Prenom onPrenom={handlePrenom} prenom={prenomEntree}/>
                            </div>
                            </div>

                            <div className="form-group">
                            <Email onEmail={handleEmail} email={emailEntree}/>                    
                            </div>

                            <div className="form-group">
                            <Texte onTexte={handleTexte} texte={texteEntree}/>
                        </div>
                    </form>

                    <button className="contact_button" type="button" className="btn btn-outline-dark" style={{fontSize:"small", width:"200px", backgroundColor: "#F08080", color:"White", borderRadius:"10px 10px 0px 10px"}} onClick={onSubmit}>ENVOYER LE MESSAGE</button>  
                    </div>
                </div>
                <div className="contact-bottom-right">
                    <img className="image" src={contactezNous} alt="contactezNous"/>
                </div>
            </div>
        </aside>
    )
}


const Nom = ({onNom, nom}) => (
    <div>
        <input type="text" 
               className="form-control" 
               placeholder="Votre nom *" 
               id ="nom" 
               style={{height:"6vh", fontSize:"3vh", marginTop:"10px"}} 
               onChange={onNom} 
               value={nom}
         />
    </div>  
 );

const Prenom = ({onPrenom,prenom}) => (
    <input type="text"
           className="form-control" 
           placeholder="Votre prenom *" 
           style={{height:"6vh", fontSize:"3vh", marginTop:"25px"}} 
           onChange={onPrenom}
           value ={prenom}
            />
);

const Email = props => (
    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Votre mail *" style={{height:"6vh", marginTop:"25px", fontSize:"3vh"}} onChange={props.onEmail} value={props.email}/>
);

const Texte = props => (
    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Votre message " onChange={props.onTexte} value={props.texte} style={{height:"110px",fontSize:"3vh", marginTop:"25px"}}></textarea>
);

export default Contact;