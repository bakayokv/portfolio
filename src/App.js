import './App.css';
import React, { useEffect } from 'react';
import {Link, Route, Switch, useLocation } from 'react-router-dom';
import Acceuil from "./Acceuil/Acceuil.js";
import Contact from "./Contact.js"
import CV from "./CV/CV.js";
import logo from "./Image/logo_small_icon_only_inverted.png"

function App() {
  const { pathname, hash } = useLocation();

  useEffect(() => {
    // if not a hash link, scroll to top
    if (hash === '') {
      window.scrollTo(0, 0);
    }
    // else scroll to id
    else {
      setTimeout(() => {
        const id = hash.replace('#', '');
        const element = document.getElementById(id);
        if (element) {
          element.scrollIntoView({ behavior: "smooth" });
        }
      }, 0);
    }
  }, [pathname]);

  return (
    <div className="site">   
        <div className="header">
          <div className="header-top">
            <div className="nav-navigation">
              <div className="Logo_option">
                <img className ="Logo" src={logo} alt="logo" />
                <div className="bootstrap_option">
                  <div class="dropdown show dropleft">
                    <a style={{backgroundColor:"#F08080"}} class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Menu
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style={{backgroundColor: "#EFEFEF"}}>
                      <a class="dropdown-item" href="/acceuil">Acceuil</a>
                      <a class="dropdown-item" href="/contact">Contact</a>
                      <a class="dropdown-item" href="/cv#cv-anchor">Experiences</a>
                      <a class="dropdown-item" href="/aPropos#apropos-anchor">A propos</a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="nav-navigation-elements">
                  <div>
                    <Link className="couleut_texte" to="/acceuil">Acceuil</Link>
                  </div>
                  <div>
                    <Link className="couleut_texte" to="/contact">Contact</Link>
                  </div>
                  <div>
                    <Link className="couleut_texte" to="/cv#cv-anchor">Experiences</Link>
                  </div>
                  <div>
                    <Link className="couleut_texte" to="/aPropos#apropos-anchor">A propos</Link>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <Switch>
          <Route path="/acceuil">
            <Acceuil/>
          </Route>

          <Route path="/contact">
            <Contact/>
          </Route>  

          <Route path="/">
            <Acceuil/>
          </Route>

        </Switch>
        <div className="last"></div>
    </div>      
  );
}

export default App;
