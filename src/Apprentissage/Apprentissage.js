import React from "react";
import './Apprentissage.css';

const bloc_1 = [
    {
      titre : "Penetration testing Prerequisities",
      description_une: "Afin de pouvoir faire un bon test, il est important de savoir comment fonctionne internet et les differentes pages web. Les differents outils tels que afin de pouvoir detecter le fonctionnement sont :",
      outil: [
          "HTTP traffic sniffing",
          "Wireshark",
          "Burpsuite"
      ],
      description_deux :"Ces outils ne sont certains parmi plusieurs." ,  
    },

    {
      titre : "System Attack",
      description_une: " Partons du principe qu'on arrive a infiltrer une machineet decouvrir les comptes et mot de passe present sur le poste. Ces mots de passes seront cripté ou comme on dit dans le jargon, hashé(crypté). C'est là que entre en scène John.",
      outil: [
         "John the ripper",
          "Hashcat",
      ]
    },


    {
      titre : "Vulnerabilty",
      description_une: " Ensuite pour trouver les vunerabilités on pourrait utiliser Nessus ou meme nmap.Nessus est un outil de sécurité informatique. Il signale les faiblesses potentielles ou avérées sur les machines testées.",
      outil: [
        "Nessus"
      ],
      description_deux :"Il est possible de detecter ces machines  et savoir memesur quelles systemes elle opperent." ,  
    },
]

const bloc_2 = [
  {
    titre : "Scanning and fingerprinting",
    description_une: "Sachant comment fonctionne internet, commençons la pratique. Afin de pouvoir infiltrer il nous faut deja les outils pour detecter les machines que l'ont veut attaquer ou les machines vulnerables. Avec les outils comme :",
    outil: [
        "Mapping a network",
        "NMAP OS Fingerprinting",
        "Mascan"
    ],
    description_deux :"il est possible de detecter ces machines  et savoir memesur quelles systemes elle opperent." ,  
    },

    {
    titre : "Web Attack",
    description_une: " Cette partie est axée sur certains outils pouvant nous aider a penetrer une application web. Nous avons : ",
    outil: [
        "Netcat",
        "Dirbuster",
        "Xss connu sous cross site scripting",
        "SQL injection",
        "SqlMap"
    ],
    description_deux :"Pour une question de syntheticité je ne detaillera pas lefonctionnement. Ils sont assez simple à utiliser mais complexe.par la meme occasion." ,  
  },

  {
    titre : "Network Attack ",
    description_une: " Pour faire simple, je vous liste les outils nous permettent d'exploiter une faille sur la machine à attaquer.",
    outil: [
        "Metasploit",
        "Meterpreter",
        "ARP poisonning"
    ],
    description_deux :"Je ne maitrise pas ces outils. Pour ceux qui connaissentpas Metasploit, il peut scanner, trouver une vulneranilité ,l'infiltrer et plus. Mais il est preferable d'utiliser des outils fait pour l'objectif dont on veut. Ex : nmap pour scanner le reseau " ,  
  },
]

const Liste_1 = bloc_1.map(item =>{  
  return(
    <section id="PSV">
      <div className="tete">
        <h4 className="txt">{item.titre}</h4>
      </div>
      <div>
        <p className="txt1">
          {item.description_une}
        </p>
        <p className="txt2">{item.outil[0]} <br/> 
                        {item.outil[1]} <br/> 
                        {item.outil[2]} 
                        
        </p>
        <p className="txt3">{item.description_deux}</p>
      </div>
   </section>
  )
}
);

const Liste_2 = bloc_2.map(item =>{ 
  return(
    <article id="WSM">
      <div className="tete">
        <h4 className="txt">{item.titre}</h4>
      </div>
      <div>
        <p className="txt1">
          {item.description_une}
        </p>

        <p className="txt2">{item.outil[0]} <br/> 
                        {item.outil[1]} <br/> 
                        {item.outil[2]} <br/> 
                        {item.outil[3]} <br/>
                        {item.outil[4]}
        </p>
        <p className="txt3">{item.description_deux}</p>
      </div>
    </article>
  )
}
);

class Apprentissage extends React.Component {
  render() {
    return (
      <section className="container">
        <hr/>
        <p style={{fontSize:"small", marginBottom:"1px"}}>Dans cette je ne vais me focus que sur les outils
          que j'ai appris et que je suis en train d'apprendre 
        </p>
        <div className="division">
          <div>{Liste_1}</div>

          <div className="eli">
            <div>{Liste_2}</div>
          </div>
        </div>
        
        <div className="visite">
          <p className="txt">
            En cas d'interet por la securite informatique, cliquer sur  
            <a style={{color :"tomato"}} href="https://my.ine.com/CyberSecurity"> INE security</a>
          </p>
        </div>
      </section>
    );
  }
}

/*
const BlocPSV = () => {
  return (
    <section className="PSV">
      <article id="penetration">
        <div className="tete">
          <h4 className="txt">Penetration testing Prerequisities</h4>
        </div>
        <div>
          <p className="txt">
          Afin de pouvoir faire un bon test, il est important de 
          savoir comment fonctionne internet et les differentes
          pages web. Les differents outils tels que afin de pouvoir
          detecter le fonctionnement sont :
          </p>
          <ul>
            <li>HTTP traffic sniffing</li>
            <li>Wireshark</li>
            <li>Burpsuite</li>
          </ul>
          <p className="txt">Ces outils ne sont certains parmi plusieurs.</p>
        </div>
        
      </article>
      
      <article id="scanning">
        <div className="tete">
          <h4 className="txt">Scanning and fingerprinting</h4>
        </div>
        <div>
          <p className="txt">
          Sachant comment fonctionne internet, commençons la<br/> pratique.
          Afin de pouvoir infiltrer il nous faut deja les outils pour <br/>
          detecter les machines que l'ont veut attaquer ou les machines <br/>
          vulnerables. Avec les outils comme :
        </p>
          <ul className="txt">
            <li>Mapping a network </li>
            <li>NMAP OS Fingerprinting</li>
            <li>Mascan</li>
          </ul>
          <p>
            il est possible de detecter ces machines  et savoir meme<br/>
            sur quelles systemes elle opperent.
          </p>
        </div>
      </article>

      <article id="vulnerability">
        <div className="tete">
          <h4 className="txt">Vunerability</h4>
        </div>
        <div>
          <p className="txt">
            Ensuite pour trouver les vunerabilités on pourrait <br/>
            utiliser Nessus ou meme nmap.Nessus est un outil de <br/>
            sécurité informatique. Il signale les faiblesses potentielles <br></br>
            ou avérées sur les machines testées.
          </p>
          <ul className="txt">
            <li>Nessus</li>
          </ul>
        </div>
      </article>
    </section>

  );
}

const BlocWSW =() => {
  return (
    <section className="WSM">
      <article id="webAttack">
        <div className="tete">
          <h4 className="txt">
            Web Attack
          </h4>
        </div>
        <p className="txt">
          Cette partie est axée sur certains outils pouvant nous aider <br/>
          a penetrer une application web. 
          Nous avons : 
        </p>
        <ul className="txt">
          <li>Netcat </li>
          <li>Dirbuster</li>
          <li>XSS connu sous Cross site scripting</li>
          <li>SQL injection</li>
          <li>SqlMap</li>
        </ul>
        <p>
          Pour une question de syntheticité je ne detaillera pas le<br/>
          fonctionnement. Ils sont assez simple à utiliser mais complexe.<br/>
          par la meme occasion.
        </p>
      </article>

      <article id="system">
        <div className="tete">
          <h4 className="txt">
            System Attack
          </h4>
        </div>
        <div>
         <p>
          Partons du principe qu'on arrive a infiltrer une machine<br/>
          et decouvrir les comptes et mot de passe present sur le poste.<br/>
          Ces mots de passes seront cripté ou comme on dit dans le jargon,<br/> 
          hashé(crypté).<br/>
          C'est là que entre en scène John.
         </p>
        <ul className="txt">
          <li>John the ripper </li>
          <li>Hashcat</li>
        </ul>
        </div>
      </article>

      <article id="networkAttack">
        <div className="tete">
          <h4 className="txt">
            Network attack
          </h4>
        </div>
        <div>
          <p className="txt">
            Pour faire simple, je vous liste les outils nous 
            permettent d'exploiter unefaille sur la machine à attaquer.<br/> 
          </p>
          <ul className="txt">
            <li>Metasploit </li>
            <li>Meterpreter</li>
            <li>ARP poisonning</li>
          </ul>
          <p>
            Je ne maitrise pas ces outils. Pour ceux qui connaissent<br/>
            pas Metasploit, il peut scanner, trouver une vulneranilité <br/>
            ,l'infiltrer et plus. Mais il est preferable d'utiliser des <br/>
            outils fait pour l'objectif dont on veut.
            Ex : nmap pour scanner le reseau 
          </p>
        </div>
      </article>
    </section>
  );
}

className Apprentissage extends React.Component {
  render() {
    return (
      <div className="apprentissage">
      <h2><a href="https://my.ine.com/CyberSecurity">INE security</a></h2>
      <p>Dans cette je ne vais me focus que sur les outils
       que j'ai appris et que je suis en train d'apprendre </p>

        <div className="bloc">
          <BlocPSV/>
          <BlocWSW/>
        </div>


        <h2>React</h2>
        <h4>The road to react by <span>Robin Wieruch</span></h4>
      </div>
    )
  }
}*/

export default Apprentissage;
