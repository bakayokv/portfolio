import React from "react";
import './Acceuil.css';
import A_propos_De_Moi from '../A_propos_de_moi/A_propos_de_moi.js';
import CV from '../CV/CV.js';


class Acceuil extends React.Component {
  render() {
    return (
      <div id="container">

          <div className="header-Bottom">
            <div className="contact-top">
              <div className="contact-top-bottom">
                  <div className="contact-top-bottom-titre">
                      <p><span className="contact_portfolio">Portfolio</span></p>
                      <ul className="contact-top-bottom-option">
                          <div className="contact-top-bottom-acceuil">
                              <li>Acceuil</li>
                          </div>
                          <div className="contact-top-bottom-contact">
                              <li>Contact</li>
                          </div>
                      </ul>
                  </div>
              </div>
            </div>
          </div>
          <A_propos_De_Moi/>
          <CV/>

      </div>
    );
  }
}

export default Acceuil;
