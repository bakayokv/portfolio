import React from "react";
import "./A_propos_de_moi.css"

class A_propos_de_moi extends React.Component {
  render() {
    return (
        <div className="aProposBloc" id="#apropos-anchor">
          <div className="bloc_a_propos">       
              <div className="bloc_moi_right">
                <div className="bloc_moi_a_propos">
                  <div>
                    <p><span className="bloc_moi_rigth_top">A propos de moi</span></p> 
                  </div>
                  <div>
                    <p>
                      <span className="texte_a_propos" style={{fontSize:"3vh"}}> Je me nomme Vafing Bakayoko. Originaire de la cote d'ivoire, je vis actuellement en france.<br/>
                      Grace aux differents voyages que j'ai pu faire(Japon, Thailande et France),<br/> j'ai pu avoir un esprit 
                      ouvert et decouvrir mes projets proffessionels et personnels.</span> 
                    </p>
                  </div>
                </div>

                <div className="bloc_moi_rigth_bottom">
                    <div className="bloc_moi_carte">
                        <div className="titre_pro">
                          <p>Projet Proffessionnel</p>
                          <hr/>
                        </div>
                        <div className="texte_pro">
                          <p>Devenir developpeur Full stack</p>
                          <p>Avoir une double competence en tant que pentester</p>
                        </div>
                      </div>
                    <div className="bloc_moi_carte">
                        <div className="titre_pro_L">
                          <p>Langues vivante</p> 
                          <hr/>   
                        </div>
                        
                        <div className="texte_pro">
                          <div>
                            <p>Francais : Langue Maternelle/Bilingue</p>
                          </div>
                          <div>
                            <p>Japonnais : Niveau élémentaire (A2)</p>
                          </div>
                          <div>
                            <p>Anglais : Niveau Proffessionnel Limité (B2)</p>
                          </div>
                        </div>
                      </div>
                    <div className="bloc_moi_carte">
                          <div>
                            <div className="titre_pro">
                              <p>Compétences</p>
                            </div>
                            <hr/>
                            <div  className="texte_pro">
                              <div>
                                <p>Front End : Javascript, Angular 7, React Js</p>
                              </div>
                              <div>
                                <p>Back End : Java, SQL.</p>
                              </div>
                              <div>
                                <p>Pentesting : Nmap, Metasploit, Meterpreter, John, XSS, SQL injection
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div className="bloc_moi_carte">
                      <div className="titre_pro">
                        <p>Systèmes</p>
                        <hr></hr>
                      </div>
                      <div className="texte_pro">
                        <div>
                          <p>Windows 7/8/9/10</p>
                        </div>
                        <div>
                          <p>Ubuntu</p>
                        </div>
                        <div>
                          <p>Windows Server</p>
                        </div>
                        <div>
                          <p>Kali Linux</p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        </div>
    );
  }
}

export default A_propos_de_moi;
