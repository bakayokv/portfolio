import React from 'react';
import "./CV.css";
import equipier from "../Image/equipier.png"
import techsupport from "../Image/techsupport.jpg"

const experiences = [
        {
          Titre : "Technicien support",
          Entreprise : "Econocom",
          Date : "*** Janvier 2021 - Maintenant ***",
          role : ["Assister le utilisateurs dans la resolution de divers incidents techniques.", 
          "Faire une prise en main a distance et travailler sur des serveurs  lorsque les serveurs des clients sont impacté",
          ]
        },
  ]; 

  const experience2 = [
    {
      Titre : "Equipier polyvalent",
      Entreprise : "Mc donald france",
      Date : "*** Mars 2019 - Aout 2020 ***",
      role : ["Gestion des caisses et pratique de la vente suggestive.", 
      "Management de l'approvisionnement des stocks.", 
      "Conduite du bon confort du client."
      ]
    }
  ]  

  const experience3 = [
    {
      Titre : "Enqueteur SNCF",
      Entreprise : "Adequat Interim",
      Date : "*** Octobre 2018 - Novemebre 2018 ***",
      role : ["Inventorier et recenser tous les passagers à bord des trains.",
       "Conduire une enquête qualitative."
      ]
    }
  ] 


  const Liste = props => props.list.map(item=>(
      <article className="bloc">
        <div className="titre">
            <h8>{item.Titre}</h8><br/>
        </div>
        <div className="articles">
          <p>
            <ul>
              <li>{item.role[0]}</li> 
              <li>{item.role[1]}</li>    
            </ul>
          </p>
        </div>
      </article>
  ));

  const Liste2 = ({list}) => list.map(item =>(
    <article className="bloc">
        <div className="titre">
          <h8>{item.Titre}</h8> <br/>
        </div>
        <div className="articles">
          <p>
            
            <ul>
              <li>{item.role[0]}</li>
              <li>{item.role[1]}</li>
              <li>{item.role[2]}</li>
            </ul>
          </p>
        </div>
    </article>
  ))

 
 
  
  const Experience = experiences.map(item => {
    return (
        <article className="Experiences">
          <Liste list={experiences}/>
          <Liste2 list={experience2}/>
          <Liste list={experience3}/>
        </article>
    )
}
)

  export default Experience;