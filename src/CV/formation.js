import React from "react"

const Formations = [
      {
            Titre : "Miage ",
            Ecole : "Miage grenoble a l'IM2FR",
            Lieu : "Grenoble",
            Date : "Septembre 2019 - Juin 2020",
            Projet : "Minishell, Tux, Voyageur Sante, Belotte",
            Langage : "Javascript, Java, XML, XSD, XSLT",
            Framework : "Angular, Firebase, Bootstrap, Node Js",
      },
      {
          Titre : "Licence 2 ",
          Ecole : "Departement Licence Science Technologie Santé",
          Lieu : "Grenoble",
          Date : "Septembre 2017 - Juin 2019",
          Projet : "Curiosity, Mini shell",
          Langage : "C, ARM, R, Java",
        },
     
        {
          Titre : "Licence 1 ",
          Ecole : "Université Joseph Fourier",
          Lieu : "Valence",
          Date : "Septembre 2016 - Juin 2017",
          Projet : "Minishell",
          Langage : "R, Java, Ocaml",
        }
  ];

  const Formation = Formations.map(item => {
        return (
            <div>
                <h3>{item.Date}</h3>
                <ul>
                  <li> {item.Titre}</li>
                  <li> {item.Ecole}</li>
                  <li> {item.Lieu}</li>
                  <li> {item.Date}</li>
                  <li> {item.Projet}</li>
                  <li>Realistation du projet avec : <span>{item.Langage}</span></li>
                </ul>
v            </div>
        )
    }
  )



  export default Formation;