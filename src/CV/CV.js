import React    from "react";
import Experience from "./Experience.js";
import './CV.css';
 

class CV extends React.Component{
  render(){
    return (
      <section className="cv" id="cv-anchor">
        <p className="expPro">Experience proffessionnelle</p>
        <p className="expProDescp">Dans la vie de tous les jours l'homme evolue selon son environnement, <br/> et les experiences qu'il a vecu.</p>
        {Experience}
      </section>
    )
    
  }
}

export default CV;

